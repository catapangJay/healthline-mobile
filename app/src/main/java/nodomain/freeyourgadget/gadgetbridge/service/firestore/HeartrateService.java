package nodomain.freeyourgadget.gadgetbridge.service.firestore;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Objects;

import nodomain.freeyourgadget.gadgetbridge.R;
import nodomain.freeyourgadget.gadgetbridge.activities.HeartRateDialog;
import nodomain.freeyourgadget.gadgetbridge.activities.HeartRateUtils;
import nodomain.freeyourgadget.gadgetbridge.entities.firestore.PatientPulse;
import nodomain.freeyourgadget.gadgetbridge.model.ActivitySample;
import nodomain.freeyourgadget.gadgetbridge.model.DeviceService;

public class HeartrateService extends Service {
    protected static final Logger LOG = LoggerFactory.getLogger(HeartrateService.class);
    FirestoreService firestoreService = FirestoreService.getInstance();

    final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (Objects.requireNonNull(intent.getAction())) {
                case DeviceService.ACTION_REALTIME_SAMPLES:
                    setMeasurementResults(intent.getSerializableExtra(DeviceService.EXTRA_REALTIME_SAMPLE));
                    break;
                default:
                    LOG.info("ignoring intent action " + intent.getAction());
                    break;
            }
        }
    };

    public HeartrateService() {

    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void setMeasurementResults(Serializable result) {
//        heart_rate_dialog_results_layout.setVisibility(View.VISIBLE);
//        heart_rate_dialog_loading_layout.setVisibility(View.GONE);
//        heart_rate_dialog_label.setText(getContext().getString(R.string.heart_rate_result));

        if (result instanceof ActivitySample) {
            ActivitySample sample = (ActivitySample) result;
//            heart_rate_hr.setVisibility(View.VISIBLE);
            if (HeartRateUtils.getInstance().isValidHeartRateValue(sample.getHeartRate())){
                try {
//                    heart_rate_widget_hr_value.setText(String.valueOf(sample.getHeartRate()));

                    PatientPulse pulse = new PatientPulse("1", sample.getHeartRate());
                    firestoreService.addPulse(pulse)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
//                                    Toast.makeText(context, "Successfully saved to firestore", Toast.LENGTH_LONG).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
//                                    Toast.makeText(context, "failed to save to firestore", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
                catch (Exception ex){
//                    Toast.makeText(context, "Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
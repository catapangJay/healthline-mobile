package nodomain.freeyourgadget.gadgetbridge.service.firestore;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import nodomain.freeyourgadget.gadgetbridge.entities.firestore.PatientNotification;
import nodomain.freeyourgadget.gadgetbridge.entities.firestore.PatientPulse;

public class FirestoreService {
    private static FirestoreService instance;
    private String patientId;

    private FirestoreService() {

    }

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public static FirestoreService getInstance() {
        if (instance == null) {
            instance = new FirestoreService();
        }
        return instance;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String id) {
        this.patientId = id;
    }

    public Task<DocumentReference> addPulse(PatientPulse pulse){
        return db.collection("HeartRates")
                .add(pulse);
    }

    public Query getNotifications(String id) {
        return db.collection("Notifications")
                .whereEqualTo("patientId", patientId)
                .whereEqualTo("alreadyViewed", false);
    }

    public Task<Void> UpdateNotifications(PatientNotification notif) {
        return db.collection("Notifications")
                .document(notif.getId())
                .update("alreadyViewed", notif.getalreadyViewed());
    }

    public Task<Void> UpsertNotifications(PatientNotification notif) {
        return db.collection("Notifications")
                .document(notif.getPatientId())
                .set(notif);
    }
}
